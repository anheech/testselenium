import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.concurrent.TimeUnit;

/*
Класс, предназначенный для проверки вызовов функций
 */
public class MainClass {
    private WebDriver driver;

    public static void main(String[] args) {
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\a.krasnova\\IdeaProjects\\testselenium\\drivers\\chromedriver.exe");
        WebDriver driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        driver.manage().window().maximize();
        //передача driver
        MainPage MainPage = new MainPage(driver);
        RegisterPage RegisterPage = new RegisterPage(driver);
        RegisterSupplierPage RegisterSupplierPage = new RegisterSupplierPage(driver);
        EtpAuthenticationRegisterPage EtpAuthenticationRegisterPage = new EtpAuthenticationRegisterPage(driver);
        //вызовы функций
        MainPage.GoToRoseltorg();
        MainPage.GoToRegisterPage();
        RegisterPage.GoToRegisterSupplierPage();
        RegisterSupplierPage.GoToSingleAccreditationPage();
        EtpAuthenticationRegisterPage.checkWarningMessage();
        driver.quit();
    }
}
