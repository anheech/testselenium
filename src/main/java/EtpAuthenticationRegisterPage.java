import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class EtpAuthenticationRegisterPage {
    private WebDriver driver;
    public EtpAuthenticationRegisterPage(WebDriver driver) {
        this.driver = driver;
    }

    private String text = "Внимание! Данная форма регистрации поставщиков предназначена только для:";
    private By warning = By.xpath("//div[(text()='" + text + "' )]");

    /*
    Проверка наличия текста: "Внимание! Данная форма регистрации поставщиков предназначена только для:"
     */
    public boolean checkWarningMessage() {
        if (driver.findElements(warning).size() == 1) {
            System.out.println("This element exist!");
            return true;
        } else return false;

    }
}
