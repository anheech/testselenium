import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.ArrayList;

/*

 */

public class MainPage{
    private WebDriver driver;
    public MainPage(WebDriver driver) {
        this.driver = driver;
    }

    private By registerButton = By.xpath("//button[@class='auth-menu__btn auth-menu__btn--register js-register-popup-link']");

    /*
    Нажатие на кнопку "Регистрация" на главной стр. Roseltorg.ru
     */
    public void GoToRegisterPage() {
        driver.findElement(registerButton).click();
        new RegisterPage(driver);
    }

    /*
    Переход на сайт Roseltorg.ru через поиск по слову ЕЭТП в поисковой системе Яндекс
     */
    public MainPage GoToRoseltorg() {
        driver.get("https://yandex.ru");
        WebElement searchInput = driver.findElement(By.id("text"));
        searchInput.sendKeys("ЕЭТП");
        searchInput.sendKeys(Keys.ENTER);
        driver.findElement(By.xpath("//b[text()='Roseltorg.ru']")).click();
        ArrayList<String> tabs2 = new ArrayList<String>(driver.getWindowHandles());
        driver.switchTo().window(tabs2.get(1));
        return new MainPage(driver);
    }
}
