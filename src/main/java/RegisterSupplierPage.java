import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class RegisterSupplierPage {
    private WebDriver driver;
    public RegisterSupplierPage(WebDriver driver) {
        this.driver = driver;
    }

    private String text = "Единая аккредитация для участия в корпоративных закупках и закупках субъектов 223-ФЗ";
    private By singleAccreditation = By.xpath("//a[text()='" + text + "']");

    /*
    Нажатие на ссылку "Единая аккредитация для участия в корпоративных закупках и закупках субъектов 223-ФЗ"
    на стр. "Регистрация в качестве поставщика"
     */
    public void GoToSingleAccreditationPage() {
        driver.findElement(singleAccreditation).click();
        new EtpAuthenticationRegisterPage(driver);
    }
}
