import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class RegisterPage {
    private WebDriver driver;
    public RegisterPage(WebDriver driver) {
        this.driver = driver;
    }

    private By registerSupplierButton = By.xpath("//a[@class='auth-window__selector-link js-supplier-register-link']");

    /*
    Нажатие на кнопку Поставщик на странице "Регистрация на площадке"
     */
    public void GoToRegisterSupplierPage() {
        driver.findElement(registerSupplierButton).click();
        new RegisterSupplierPage(driver);
    }
}
