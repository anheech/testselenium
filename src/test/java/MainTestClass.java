import org.junit.*;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.concurrent.TimeUnit;

/*
Главный тестовый класс
 */
public class MainTestClass {
    private WebDriver driver;
    private MainPage mainPage;
    private RegisterPage registerPage;
    private RegisterSupplierPage registerSupplierPage;
    private EtpAuthenticationRegisterPage etpAuthenticationRegisterPage;

    /*
    Инициализация драйвера
     */
    @Before
    public void setUp() {
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\a.krasnova\\IdeaProjects\\testselenium\\drivers\\chromedriver.exe"); //путь к локальному chromedriver.exe
        driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);

        driver.manage().window().maximize();

        mainPage = new MainPage(driver);
        registerPage = new RegisterPage(driver);
        registerSupplierPage = new RegisterSupplierPage(driver);
        etpAuthenticationRegisterPage = new EtpAuthenticationRegisterPage(driver);
    }

    /*
    Основной тест
     */
    @Test
    public void checkWarningMessage() {
        mainPage.GoToRoseltorg();
        mainPage.GoToRegisterPage();
        registerPage.GoToRegisterSupplierPage();
        registerSupplierPage.GoToSingleAccreditationPage();
        Assert.assertTrue(etpAuthenticationRegisterPage.checkWarningMessage());
    }

    @After
    public void tearDown() {
        driver.quit();
    }

}
